/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.aip.assg1.controller;

import java.io.IOException;
import java.io.OutputStream;
import uts.aip.assg1.model.PostDTO;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.*;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import uts.aip.assg1.database.PostDAO;

/**
 *
 * @author sunda
 * 
 * This class is the domain logic of post
 */
@Named
@RequestScoped
public class PostContorller implements Serializable {

    private int id;
    private String title;
    private String type;
    private String city;
    private int rent;
    private String suburb;
    private String detail;
    private String date;
    private String contact;
    private String email;
    private String publisher;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getRent() {
        return rent;
    }

    public void setRent(int rent) {
        this.rent = rent;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    /**
     * get all texts from the form and use postDAO to save them
     * 
     * @return go to myrecord.xhtml
     */
    public String saveAsNew() {
        PostDAO postDAO = new PostDAO();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        publisher = request.getRemoteUser();
        date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        postDAO.save(city, contact, date, email, rent, suburb, title, type, publisher, detail);
        System.out.println(city);
        return "myrecord";
    }

    /**
     * 
     * @return all posts from database
     */
    public ArrayList<PostDTO> getPosts() {
        PostDAO postDAO = new PostDAO();
        return postDAO.findAll();
    }

    /**
     * 
     * @param username input a username
     * @return all the posts posted by that user
     */
    public ArrayList<PostDTO> getPosts(String username) {
        System.out.println("username= " + username);
        PostDAO postDAO = new PostDAO();
        return postDAO.findAll(username);
    }

    /**
     * retrieve a specific DTO and set it to corresponding fields
     * 
     * @param index the primary key of post
     */
    public void loadPost(int index) {
        PostDAO postDAO = new PostDAO();
        PostDTO post = postDAO.read(index);
        id = post.getId();
        title = post.getTitle();
        type = post.getType();
        city = post.getCity();
        rent = post.getRent();
        suburb = post.getSuburb();
        detail = post.getDetail();
        date = post.getDate();
        contact = post.getContact();
        email = post.getEmail();
        publisher = post.getPublisher();

    }

    /**
     * get all texts from the form and use postDAO to update the database
     * 
     * @return go to myrecord.xhtml
     */
    public String saveChanges() {
        PostDAO postDAO = new PostDAO();
        date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        postDAO.update(id, city, contact, date, email, rent, suburb, title, type, detail);
        return "myrecord";
    }

    /**
     * delete a post by its id
     * 
     * @param id post id
     * @return null, not going to other page
     */
    public String deletePost(int id) {
        PostDAO postDAO = new PostDAO();
        postDAO.delete(id);
        return null;
    }

    /**
     * trigger the generateCSV() to download CSV
     * 
     * @return null, not going to other page
     */
    public String downloadCSV() {
        try {
            generateCSV();
        } catch (IOException ex) {
            Logger.getLogger(PostContorller.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
/**
 * read all posts from database
 * then write them one by one to the csv file
 * 
 * @throws IOException 
 * 
 */
    private void generateCSV() throws IOException {
        PostDAO postDAO = new PostDAO();
        String filename = "flatmates.csv";
        String line = "Suburb, City, RoomType, Rent, Name, Email, PublishedDate\n";
        
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();

        ec.responseReset();
        ec.setResponseContentType("text/csv");
        //ec.setResponseContentLength(line.length());
        ec.setResponseHeader("Content-Disposition", "attachment;filename=" + filename);

        OutputStream os = ec.getResponseOutputStream();

        os.write(line.getBytes());
        for(PostDTO post: postDAO.findAll()){
            line = post.getSuburb() + "," + post.getCity() + "," + post.getType()+ "," +
                    post.getRent()+ "," + post.getContact()+ "," + post.getEmail()+ "," +
                    post.getDate() + "\n";
            os.write(line.getBytes());
        }
        os.flush();
        os.close();

        fc.responseComplete();
    }
}
