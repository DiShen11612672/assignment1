/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.aip.assg1.controller;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import uts.aip.assg1.Sha;
import uts.aip.assg1.database.UserDAO;

/**
 *
 * @author sunda
 * 
 * This class is for resetting the password
 */
@Named
@RequestScoped
public class PasswordController implements Serializable {

    private String password;
    private String newPassword;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * encrypt the password and match it with database,
     * if it is correct, then change it to new one.
     * 
     * @return null, no redirecting to other page
     */
    public String changePsw() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        String username = request.getRemoteUser();
        UserDAO userDAO = new UserDAO();
        String shaPassword = null;
        String shaNewPassword = null;
        try {
            shaPassword = Sha.hash256(password);
            shaNewPassword = Sha.hash256(newPassword);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(PasswordController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(userDAO.matchPassword(username,shaPassword)){
            userDAO.changePassword(username, shaNewPassword);
            context.addMessage(null, new FacesMessage("Password has been changed successfully."));
        }else{
            context.addMessage(null, new FacesMessage("The original password is incorrect, please try again."));
        }
        return null;
    }

}
