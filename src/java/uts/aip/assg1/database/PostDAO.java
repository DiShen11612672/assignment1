/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uts.aip.assg1.database;

import java.io.IOException;
import java.io.OutputStream;
import uts.aip.assg1.model.PostDTO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 *
 * @author sunda
 * 
 * This class is the data source of post
 */
public class PostDAO {

    /**
     * read a post by its id and set it to DTO
     * @param index
     * @return a postDTO if found
     */
    public PostDTO read(int index) {
        String query = "select * " + "from posts " + "where id = ?";
        PostDTO post = null;
        DataSource ds = null;
        try {
            ds = (DataSource) InitialContext.doLookup("jdbc/aip");
        } catch (NamingException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection conn = ds.getConnection();
                PreparedStatement ps = conn.prepareStatement(query);) {
            ps.setInt(1, index);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                post = new PostDTO();
                post.setId(rs.getInt("id"));
                post.setCity(rs.getString("city"));
                post.setContact(rs.getString("contact"));
                post.setDate(rs.getString("publishDate"));
                post.setEmail(rs.getString("email"));
                post.setRent(rs.getInt("rent"));
                post.setSuburb(rs.getString("suburb"));
                post.setTitle(rs.getString("title"));
                post.setType(rs.getString("roomType"));
                post.setPublisher(rs.getString("publisher"));
                post.setDetail(rs.getString("detail"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return post;
    }
/**
 * save all the info to database
 * 
 * @param city
 * @param contact
 * @param date
 * @param email
 * @param rent
 * @param suburb
 * @param title
 * @param type
 * @param publisher
 * @param detail 
 */
    public void save(String city, String contact, String date, String email, int rent, String suburb, String title, String type, String publisher, String detail) {
        String query = "INSERT INTO posts "
                + "(title, roomType, city, rent, suburb, detail, publishDate, contact, email, publisher) " + "VALUES "
                + "(?,?,?,?,?,?,?,?,?,?)";
        DataSource ds = null;
        try {
            ds = (DataSource) InitialContext.doLookup("jdbc/aip");
        } catch (NamingException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection conn = ds.getConnection();
                PreparedStatement ps = conn.prepareStatement(query);) {
            ps.setString(1, title);
            ps.setString(2, type);
            ps.setString(3, city);
            ps.setInt(4, rent);
            ps.setString(5, suburb);
            ps.setString(6, detail);
            ps.setString(7, date);
            ps.setString(8, contact);
            ps.setString(9, email);
            ps.setString(10, publisher);
            try {
                ps.executeUpdate();
            } catch (Exception e) {
                Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, e);
            }

        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
/**
 * retrieve all posts from database
 * 
 * @return a collection of found postDTO object
 */
    public ArrayList<PostDTO> findAll() {
        String query = "select * from posts";
        ArrayList<PostDTO> posts = new ArrayList<PostDTO>();
        PostDTO post = null;
        DataSource ds = null;
        try {
            ds = (DataSource) InitialContext.doLookup("jdbc/aip");
        } catch (NamingException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection conn = ds.getConnection();
                Statement stat = conn.createStatement();
                ResultSet rs = stat.executeQuery(query)) {
            while (rs.next()) {
                post = new PostDTO();
                post.setId(rs.getInt("id"));
                post.setCity(rs.getString("city"));
                post.setContact(rs.getString("contact"));
                post.setDate(rs.getString("publishDate"));
                post.setEmail(rs.getString("email"));
                post.setRent(rs.getInt("rent"));
                post.setSuburb(rs.getString("suburb"));
                post.setTitle(rs.getString("title"));
                post.setType(rs.getString("roomType"));
                post.setPublisher(rs.getString("publisher"));
                post.setDetail(rs.getString("detail"));
                posts.add(post);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return posts;
    }
/**
 * update a post by its id
 * 
 * @param id
 * @param city
 * @param contact
 * @param date
 * @param email
 * @param rent
 * @param suburb
 * @param title
 * @param type
 * @param detail 
 */
    public void update(int id, String city, String contact, String date, String email, int rent, String suburb, String title, String type, String detail) {
        String query = "UPDATE posts "
                + "SET city = ?, contact = ?, publishDate = ?, email = ?, rent = ?, suburb = ?, title = ?, roomType = ?, detail = ?"
                + "WHERE id = ?";
        DataSource ds = null;
        try {
            ds = (DataSource) InitialContext.doLookup("jdbc/aip");
        } catch (NamingException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection conn = ds.getConnection();
                PreparedStatement ps = conn.prepareStatement(query);) {
            ps.setString(1, city);
            ps.setString(2, contact);
            ps.setString(3, date);
            ps.setString(4, email);
            ps.setInt(5, rent);
            ps.setString(6, suburb);
            ps.setString(7, title);
            ps.setString(8, type);
            ps.setString(9, detail);
            ps.setInt(10, id);

            try {
                ps.executeUpdate();
            } catch (Exception e) {
                Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, e);
            }

        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
/**
 * delete a post from database by id
 * 
 * @param id 
 */
    public void delete(int id) {
        String query = "DELETE FROM posts " + "WHERE id = ?";
        DataSource ds = null;
        try {
            ds = (DataSource) InitialContext.doLookup("jdbc/aip");
        } catch (NamingException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection conn = ds.getConnection();
                PreparedStatement ps = conn.prepareStatement(query);) {
            ps.setInt(1, id);
            try {
                ps.executeUpdate();
            } catch (Exception e) {
                Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, e);
            }

        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
/**
 * retrieve all posts that posted by the user
 * @param username
 * @return a collection of found postDTO object
 */
    public ArrayList<PostDTO> findAll(String username) {
        String query = "select * from posts " + "where publisher = ?";
        ArrayList<PostDTO> posts = new ArrayList<PostDTO>();
        PostDTO post = null;
        DataSource ds = null;
        try {
            ds = (DataSource) InitialContext.doLookup("jdbc/aip");
        } catch (NamingException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Connection conn = ds.getConnection();
                PreparedStatement ps = conn.prepareStatement(query);) {
            ps.setString(1, username);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                post = new PostDTO();
                post.setId(rs.getInt("id"));
                post.setCity(rs.getString("city"));
                post.setContact(rs.getString("contact"));
                post.setDate(rs.getString("publishDate"));
                post.setEmail(rs.getString("email"));
                post.setRent(rs.getInt("rent"));
                post.setSuburb(rs.getString("suburb"));
                post.setTitle(rs.getString("title"));
                post.setType(rs.getString("roomType"));
                post.setPublisher(rs.getString("publisher"));
                post.setDetail(rs.getString("detail"));
                posts.add(post);
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return posts;
    }

}
