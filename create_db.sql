create table accounts(
username varchar(255) not null primary key,
password varchar(255) not null
);

create table posts(
id int not null primary key generated always as identity,
title varchar(255) not null,
roomType varchar(255) not null,
city varchar(255) not null,
rent int not null,
suburb varchar(255) not null,
detail varchar(255),
publishDate varchar(255) not null,
contact varchar(255) not null,
email varchar(255) not null,
publisher varchar(255) not null
);


create view jdbcrealm_user (username, password) as
select username, password
from accounts;

create view jdbcrealm_group (username, groupname) as
select username, 'Users'
from accounts;

select * from posts